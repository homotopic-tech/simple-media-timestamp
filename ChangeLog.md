# Changelog for simple-media-timestamp

## v0.2.0.0

* Use non-plural fields.

## v0.1.0.0

* Add `Time` and `Range` types.

-- |
--   Module    : Media.Timestamp
--   License   : MIT
--   Stability : experimental
--
-- A simple timestamp type.
module Media.Timestamp
  ( Time (..),
    Range (..),
  )
where

-- | Timestamp data type.
--
-- @since 0.2.0.0
data Time = Time
  { hour :: Int,
    minute :: Int,
    second :: Int,
    millisecond :: Int
  }
  deriving (Eq, Ord, Show)

-- | Interval of two timestamps.
--
-- @since 0.1.0.0
data Range = Range
  { from :: Time,
    to :: Time
  }
  deriving (Eq, Ord, Show)
